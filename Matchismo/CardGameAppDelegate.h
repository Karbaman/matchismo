//
//  CardGameAppDelegate.h
//  Matchismo
//
//  Created by Petr Lazarev on 19/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
