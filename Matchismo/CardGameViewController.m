//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Petr Lazarev on 19/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (strong, nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gameModeSelector;
@end

@implementation CardGameViewController

- (CardMatchingGame *) game
{
    if (!_game)
        _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[self createDeck]];
    return _game;
}

- (Deck *) createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (IBAction)touchCardButton:(UIButton *)sender
{
    [self.gameModeSelector setEnabled:NO];
    int choosenButtonIndex = [self.cardButtons indexOfObject:sender];
    
    NSUInteger maxChosenCardsCount = [self.gameModeSelector selectedSegmentIndex] + 1;
    [self.game chooseCardAtIndex:choosenButtonIndex maxChosenCardsCount:maxChosenCardsCount];
    [self updateUI];
}

- (void) updateUI
{
    for (UIButton *cardButton in self.cardButtons)
    {
        int cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setTitle:[self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self imageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
}

- (NSString *) titleForCard:(Card *)card
{
    return card.isChosen ? card.contents : @"";
}

- (UIImage *) imageForCard:(Card *)card
{
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback" ];
}

- (IBAction)replayGame {
    [self.gameModeSelector setEnabled:YES];
    _game = Nil;
    for (int index=0; index<[self.cardButtons count]; index++)
    {
        Card *card = [self.game cardAtIndex:index];
        card.chosen = NO;
        card.matched = NO;
    }
    [self updateUI];
}


@end
