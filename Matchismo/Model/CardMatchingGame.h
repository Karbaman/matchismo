//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Petr Lazarev on 24/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject
@property (nonatomic, readonly) NSInteger score;

// designated initializer
- (instancetype) initWithCardCount:(NSUInteger)count
                         usingDeck:(Deck *) deck;

- (void) chooseCardAtIndex:(NSUInteger)index maxChosenCardsCount:(NSUInteger) maxChosenCardsCount;
- (Card *) cardAtIndex:(NSUInteger) index;
@end
