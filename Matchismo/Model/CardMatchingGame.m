//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Petr Lazarev on 24/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray *cards; // of Card
@end

@implementation CardMatchingGame

static const int MISMATCH_PENALTY = 2;
static const int MATCH_BONUS = 4;
static const int COST_TO_CHOOSE = 1;

- (NSMutableArray *) cards
{
    if (!_cards)
        _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (instancetype) initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck
{
    self = [super init];
    if (self)
    {
        for (int i=0; i<count; i++)
        {
            Card *card = [deck drawRandomCard];
            if (card)
                [self.cards addObject:card];
            else
            {
                self = nil;
                break;
            }
        }
    }
    return self;
}

- (Card *) cardAtIndex:(NSUInteger)index
{
    return index < [self.cards count] ? self.cards[index] : nil;
}

- (void) chooseCardAtIndex:(NSUInteger)index maxChosenCardsCount:(NSUInteger) maxChosenCardsCount
{
    Card *card = [self cardAtIndex:index];
    if (!card.isMatched)
    {
        if (card.isChosen)
            card.chosen = NO;
        else
        {
            NSMutableArray *chosenCards = [[NSMutableArray alloc] init];
            // find all chosen cards
            for (Card *otherCard in self.cards)
            {
                if (otherCard.isChosen && !otherCard.isMatched)
                    [chosenCards addObject:otherCard];
            }
            
            if ([chosenCards count] == maxChosenCardsCount)
            {
                int matchScore = [card match:chosenCards];
                if (matchScore)
                {
                    self.score += matchScore*MATCH_BONUS;
                    card.matched = YES;
                    for (Card *otherCard in chosenCards)
                        otherCard.matched = YES;

                }
                else
                {
                    self.score -= MISMATCH_PENALTY;
                    for (Card *otherCard in chosenCards)
                        otherCard.chosen = NO;
                }
            }
            
            self.score -= COST_TO_CHOOSE;
            card.chosen = YES;
        }
    }
}

@end
