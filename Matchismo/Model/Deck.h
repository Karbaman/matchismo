//
//  Deck.h
//  Matchismo
//
//  Created by Petr Lazarev on 24/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

- (void) addCard:(Card *)card atTop:(BOOL)atTop;
- (void) addCard:(Card *)card;
- (Card *)drawRandomCard;

@end
