//
//  PlayingCard.h
//  Matchismo
//
//  Created by Petr Lazarev on 22/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *) validSuits;
+ (NSUInteger) maxRank;

@end
