//
//  PlayingCard.m
//  Matchismo
//
//  Created by Petr Lazarev on 22/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

- (NSString *) contents
{
    NSArray *rankStrings = [PlayingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit; // because we provide both getter and setter

+ (NSArray *) validSuits
{
    return @[@"♠︎", @"♣︎", @"♥︎", @"♦︎"];
}

+ (NSUInteger) maxRank
{
    return [[PlayingCard rankStrings] count];
}

- (void) setRank:(NSUInteger)rank
{
    if (rank == 0)
        NSLog(@"ERROR:ZERO RANK:%d", rank);
    if (rank < [PlayingCard maxRank])
        _rank = rank;
    else
        NSLog(@"ERROR:RANK IS NOT VALID:%d", rank);
}

- (void) setSuit:(NSString *)suit
{
    if ([[PlayingCard validSuits] containsObject:suit])
        _suit = suit;
}

- (NSString *) suit
{
    return _suit ? _suit : @"?";
}

+ (NSArray *) rankStrings
{
    return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6",
             @"7", @"8", @"9", @"10", @"J", @"Q", @"K"];
}

- (int)match:(NSArray *)otherCards
{
    int score = 0;
    for (PlayingCard *otherCard in otherCards)
    {
        if (otherCard.rank == self.rank)
            score += 4;
        else if ([otherCard.suit isEqualToString:self.suit])
            score += 1;
    }
    return score;
}

@end
