//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Petr Lazarev on 24/02/14.
//  Copyright (c) 2014 JetFork. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
